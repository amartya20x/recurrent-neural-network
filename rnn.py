import theano
import theano.tensor as T
import numpy as np
import cPickle
import random
import matplotlib.pyplot as plt

class RNN(object):

	def __init__(self, nin, n_hidden, nout):
		rng = np.random.RandomState(124)
		W_uh = np.asarray(
			rng.normal(size=(n_hidden, nin), scale= .01, loc = 0.0), dtype = theano.config.floatX)
		W_hh = np.asarray(
			rng.normal(size=(n_hidden, n_hidden), scale=.01, loc = 0.0), dtype = theano.config.floatX)
		W_hy = np.asarray(
			rng.normal(size=(n_hidden, nout), scale =.01, loc=0.0), dtype = theano.config.floatX)
		b_hh = np.zeros((n_hidden,), dtype=theano.config.floatX)
		b_hy = np.zeros((nout,), dtype=theano.config.floatX)
		self.activ = T.tanh
		lr = T.scalar()
		u = T.matrix()
		t = T.ivector('t')
                
		W_uh = theano.shared(W_uh, 'W_uh')
		W_hh = theano.shared(W_hh, 'W_hh')
		W_hy = theano.shared(W_hy, 'W_hy')
		b_hh = theano.shared(b_hh, 'b_hh')
		b_hy = theano.shared(b_hy, 'b_hy')

		h0_tm1 = theano.shared(np.zeros(n_hidden, dtype=theano.config.floatX))

		h,_ = theano.scan(self.recurrent_fn, sequences = T.transpose(u),
					   outputs_info = [h0_tm1],
					   non_sequences = [W_hh, W_uh, W_hy, b_hh,b_hy])
                prod =  T.dot(h[h.shape[0]-1], W_hy) + b_hy
		p_y_given_x = T.nnet.softmax(prod)
		#cost = T.mean((p_y_given_x-T.transpose(t))**2)
                #cost = T.sum((p_y_given_x[0:] - t)**2)
                cost = -T.sum(T.log(np.abs(p_y_given_x))[0][T.argmax(t)])
                y_pred = T.argmax(p_y_given_x, axis=1)
                #cost = -T.mean(T.log(p_y_given_x)[t])
                gW_hh, gW_uh, gW_hy,\
		   gb_hh, gb_hy = T.grad(
			   cost, [W_hh, W_uh, W_hy, b_hh, b_hy])

		self.train_step = theano.function([u, t, lr], cost,
							on_unused_input='warn',
							updates=[(W_hh, W_hh - lr*gW_hh),
									 (W_uh, W_uh - lr*gW_uh),
									 (W_hy, W_hy - lr*gW_hy),
									 (b_hh, b_hh - lr*gb_hh),
									 (b_hy, b_hy - lr*gb_hy)],
							allow_input_downcast=True)
		self.return_out = theano.function([u],y_pred,on_unused_input='warn')
	def recurrent_fn(self, u_t, h_tm1, W_hh, W_uh, W_hy, b_hh,b_hy):
		h_t = self.activ(T.dot(h_tm1, W_hh) +  T.dot( W_uh,T.transpose(u_t)) +  b_hh)
                return h_t
